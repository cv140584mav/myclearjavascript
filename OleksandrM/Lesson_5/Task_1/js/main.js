var total = 0;
var resultElement = document.getElementById("result");

function getRndNumber(){
    return Math.floor((Math.random()*6) + 1);
};

function print (someText){
    resultElement.innerHTML += someText;};

function isNumbersEqual(first, second){
    if(first == second){
    return print("Выпал дубль. Число " + first + "<br>");
    };
}

function isBigDifference(first, second){
    if(first < 3 && second > 4){
    return print("Большой разброс между костями. Разница составляет " + (second - first) + "<br>");
    };
}

function getTotal(){
    resultElement.innerHTML += total > 100 ? "Победа, Вы набрали " + total + " очков" : "Вы проиграли, у Вас " + total + " очков";
};

function run(){
    for (i = 1; i <= 15; i++){
        if (i == 8 || i == 13){continue};
        var first = getRndNumber();
        var second = getRndNumber();
        print ("Первая кость: " + first + " Вторая кость: " + second + "<br>");
        isNumbersEqual(first, second);
        isBigDifference(first, second);
        total += first + second;  
    };
   getTotal();  
};
run();
